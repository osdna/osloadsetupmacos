# osloadsetupmacos


The application osloadsetupmacos allows the automatic execution of some commands during the opening of the user's session automatically.

This application then offers the user the possibility of filling some gaps in the customization of the configuration of macOS.
<br>
The application allows:

- Disable Bluetooth
- Disable WIFI
- Reset thumbnail disk cache
- Disable IPV6 on all interfaces

## Requirements



The app requires some access and execution rights that are required by macOS and when it is first launched.
The permissions to be set are notably defined in the System Preferences of macOS.
When opening for the first time, the preferences of the modified access rights will be displayed to the user.

- [How do I enable Accessibility permissions on macOS/X ?](https://help.rescuetime.com/article/59-how-do-i-enable-accessibility-permissions-on-mac-osx) 

In order to be able to run the application at each session opening, the user must add it in the opening section of the system preferences.


## Use



Once installed and executing, no specific action is required from the user.
 

## Tests



#### Hardware / Software


<span style="color:red">Information - The tests as well as the development of the application have been carried out with the versions of the following tools and systems: </span>

  

- AppleScript 2.7 : Version 2.11 (208)

- System : macOS 10.15.7 (19H114)

  
## Maintainers



Current maintainer :

* osxcode - [https://gitlab.com/osxcode/osloadsetupmacos](https://gitlab.com/osxcode/osloadsetupmacos)


## Privacy



<span style="color:red">The application osloadsetupmacos does not collect or store any personal data about the user.</span>


## Warning



It is the end user's responsibility to obey all applicable local, state and federal laws.

The author is in no way responsible for any misuse or damage caused by this program.

  
## Support



If you want you can support me ;)

[-- OXEN --](https://oxen.io)


<img src="https://osxcode.gitlab.io/os/img/QRCODE_OXEN_WALLET.png"  width="500" height="350">


## Licence  



[LGPL - Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html)
